VENV_DIR ?= venv

prepare:
	pip install virtualenv
	python3.8 -m venv ${VENV_DIR}
	. ${VENV_DIR}/bin/activate; \
	pip install -r requirements.txt

run:
	. ${VENV_DIR}/bin/activate; \
	python3.8 src/main.py