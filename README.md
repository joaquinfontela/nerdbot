# NerdBot 

## Set up 
The goal of this file is to give information about how to set up the project for running it locally.

### Prerequisites
* Install Python (3.8 or higher)
* Install make

### Open AI API Key
In order to be able to run the project, you need to have an Open AI API key in the `OPENAI_API_KEY` environment variable.
To achieve this:
* Create an account in https://openai.com/blog/openai-api.
* Click on `Sign Up` and follow the steps until you have created an account.
* Visit `https://platform.openai.com/account/api-keys` and click on `Create new secret key`.
* Add a name for the key, click on `Create secret key` and copy the key.
* Create a `.env` file in the root of the project and add the following line:
```
OPEN_AI_API_KEY=<your key>
```

### Preparing the input
* Inside the `.env` file, add the following lines:
```
USERNAME=<your username>
TWEET_CONTENT=<your tweet>
```
* You can follow the example in `.env.example` file.

### Running the project
* Execute `make prepare` (this installs all the libraries in a `venv`)
* Execute `make run`
The app will output the input tweet and the bot response in JSON format.