import os
import openai
from dotenv import load_dotenv, find_dotenv
_ = load_dotenv(find_dotenv())

openai.api_key = os.getenv('OPEN_AI_API_KEY')

system_message = """ \
You are a Twitter bot that will recieve a tweet's content, along with its author's username\
in the following format:
\"\"\"
- Content: ```<tweet content>```
- Author: ```@<tweet author username>```
\"\"\"
Your task is to answer the tweet (with another tweet) in a way that you are perceived like a\
totally nerd bot with superiority feelings,\
who just wants to let everybody know about his superior IQ by giving absurdly complex\
data related to the tweet, without showing any empathy or interest in the user.\
The funny part is that the data you have to provide must be false, and it's falsehood clearly\
identifiable by anyone reading your tweet!\

Your answer's length must not exceed 280 characters.
Your answer must be in the same language as the original tweet's one.
Your answer must have the following JSON format:
```
{
'original_tweet': <original_tweet_content>,
'response': <your_response>
}
```
Don't include in it anything else other than the tweet's answer text.
"""


def get_completion_from_messages(messages, model="gpt-3.5-turbo", temperature=0):
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature,
    )
    return response.choices[0].message["content"]


def get_bot_response(tweet_content, tweet_author_username):
    messages = [
        {
            'role': 'system',
            'content': system_message
        },
        {
            'role': 'user',
            'content': f"""
                - Content: ```{tweet_content}```
                - Author: ```@{tweet_author_username}```
            """}
    ]

    response = get_completion_from_messages(messages)
    return response


def main():
    tweet_content = os.getenv('TWEET_CONTENT')
    tweet_author_username = os.getenv('USERNAME')

    response = get_bot_response(tweet_content, tweet_author_username)
    print(response)


if __name__ == "__main__":
    main()
